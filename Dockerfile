FROM openjdk:12-jdk-alpine
VOLUME /tmp
RUN apk --no-cache add tzdata
ENV TZ=America/Bogota
ENV AWS_IAM_ACCOUNT_SECRET_KEY=${enviroment.AWS_IAM_ACCOUNT_SECRET_KEY}
ENV AWS_IAM_ACCOUNT_ACCESS_KEY=${enviroment.AWS_IAM_ACCOUNT_ACCESS_KEY}
ENV REGION_CODE=${enviroment.REGION_CODE}
ENV JAVA_HOME /opt/jdk
ENV PATH $JAVA_HOME/bin:$PATH
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ADD ./build/libs/*.jar app.jar
ENTRYPOINT ["java","-Djava.awt.headless=true","-Duser.timezone=America/Bogota","-jar","/app.jar"]


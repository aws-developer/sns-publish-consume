package co.com.sns.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.sns.business.SnsBusiness;
import co.com.sns.domain.request.ClienteExisteArnTopicRequest;
import co.com.sns.domain.request.EmailArnTopicRequest;
import co.com.sns.domain.response.Response;

@RestController
@RequestMapping("/sns-service")
public class CoreRest {
	
	private SnsBusiness sns;
	
	@Autowired
	public void setSnsBusiness(SnsBusiness sns) {
		this.sns = sns;
	}
	
	@PostMapping(value = "/add-suscription-email")
	public Response<Boolean> addSubscriptionEmail(@RequestBody @Valid final EmailArnTopicRequest request){
		return sns.addSubscriptionEmail(request);
	}
	
	@PostMapping(value = "/publish-message-arn-topic")
	public Response<Boolean> publishMessageEmailArnTopic(@RequestBody @Valid final ClienteExisteArnTopicRequest request)  {
		return sns.publishMessageEmailArnTopic(request);
	}
}

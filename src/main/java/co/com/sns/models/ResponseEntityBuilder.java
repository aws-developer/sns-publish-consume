package co.com.sns.models;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import co.com.sns.domain.response.Response;

public class ResponseEntityBuilder {
	public static ResponseEntity<Object> build(Response<Object> err) {
		return new ResponseEntity<Object>(err, HttpStatus.valueOf(err.getStatus()));
	}
}

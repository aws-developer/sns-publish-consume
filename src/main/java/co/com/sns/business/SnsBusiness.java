package co.com.sns.business;

import co.com.sns.domain.request.ClienteExisteArnTopicRequest;
import co.com.sns.domain.request.EmailArnTopicRequest;
import co.com.sns.domain.response.Response;

public interface SnsBusiness {
	Response<Boolean> addSubscriptionEmail(EmailArnTopicRequest request);
	Response<Boolean> publishMessageEmailArnTopic(ClienteExisteArnTopicRequest request);
}

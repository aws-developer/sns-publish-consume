package co.com.sns.business;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.google.gson.Gson;

import co.com.sns.utils.Util;
import co.com.sns.domain.dto.Message;
import co.com.sns.domain.request.ClienteExisteArnTopicRequest;
import co.com.sns.domain.request.DefaultPublishRequest;
import co.com.sns.domain.request.DefaultSuscriptionRequest;
import co.com.sns.domain.request.EmailArnTopicRequest;
import co.com.sns.domain.response.Response;

@Service
public class SnsBusinessImpl implements SnsBusiness {

	private AmazonSNSClient amazonSNSClient;

	@Autowired
	public void setAmazonSNSClient(AmazonSNSClient amazonSNSClient) {
		this.amazonSNSClient = amazonSNSClient;
	}

	@Override
	public Response<Boolean> addSubscriptionEmail(EmailArnTopicRequest request) {
		
		DefaultSuscriptionRequest payload = new DefaultSuscriptionRequest();
		payload.setArnTopic(request.getArnTopic());
		payload.setType("email");
		payload.setPayload(request.getEmail());
		
		return suscriptionDefault(payload);
	}

	@Override
	public Response<Boolean> publishMessageEmailArnTopic(ClienteExisteArnTopicRequest request) {
		Message msn = new Message();
		msn.setId(request.getClienteExiste().getId());
		msn.setTipoId(request.getClienteExiste().getTipoId());

		DefaultPublishRequest payload = new DefaultPublishRequest();
		payload.setArnTopic(request.getArnTopic());
		payload.setPayload(new Gson().toJson(msn));
		
		return publishMessageDefault(payload);
	}

	public Response<Boolean> suscriptionDefault(@Valid DefaultSuscriptionRequest request) {
		final SubscribeRequest subscribeRequest = new SubscribeRequest(request.getArnTopic(), request.getType(),
				request.getPayload());
		amazonSNSClient.subscribe(subscribeRequest);
		return Util.callResponse("Suscription Success", true);
	}
	
	public Response<Boolean> publishMessageDefault(@Valid DefaultPublishRequest request) {

		final PublishRequest publishRequest = new PublishRequest(request.getArnTopic(), request.getPayload());
		amazonSNSClient.publish(publishRequest);

		return Util.callResponse("Publish Message Success", true);
	}
	
	
	
	

}

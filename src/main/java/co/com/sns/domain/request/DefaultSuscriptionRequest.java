package co.com.sns.domain.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import co.com.sns.validators.EnumNamePattern;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class DefaultSuscriptionRequest {
	@EnumNamePattern(regexp = "email|http|https", message ="Valid values: email, http, https.")
	@NotNull(message = "arnTopic is required.")
	private String type;
	
	@Pattern(regexp = "^[a-zA-Z0-9.:#_-]+$", message= "Error, invalid arnTopic.")
	@NotNull(message = "arnTopic is required.")
	private String arnTopic;
	
	@NotNull(message = "payload is required.")
	private String payload;
}

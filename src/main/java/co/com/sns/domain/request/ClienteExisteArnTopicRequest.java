package co.com.sns.domain.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.Valid;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ClienteExisteArnTopicRequest {

	@Pattern(regexp = "^[a-zA-Z0-9.:_-]+$", message= "Error, invalid arnTopic.")
	@NotNull(message = "arnTopic is required.")
	private String arnTopic;
	
	@Valid
	@NotNull(message = "clienteExiste is required.")
	private ClienteExistePNRequestDTO clienteExiste;
}

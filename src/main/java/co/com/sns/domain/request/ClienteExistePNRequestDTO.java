package co.com.sns.domain.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter 
@Setter
@AllArgsConstructor 
@NoArgsConstructor
public class ClienteExistePNRequestDTO {
	
	@Pattern(regexp = "^[0-9]+$", message= "tipoId is numeric.")
	@Length(min=1,max=2, message="tipoId min 1, max 2 characters.")
	@NotNull(message = "tipoId is required.")
	private String tipoId;

	@Pattern(regexp = "^[0-9]+$", message= "id is numeric.")
	@NotNull(message = "id is required.")
	private String id;
}

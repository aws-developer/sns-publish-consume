package co.com.sns.domain.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class EmailArnTopicRequest {

	@Pattern(regexp = "^[a-zA-Z0-9.:#_-]+$", message= "Error, invalid arnTopic.")
	@NotNull(message = "arnTopic is required.")
	private String arnTopic;
	
	@Pattern(regexp = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$", message= "Error, invalid email.")
	@NotNull(message = "email is required.")
	@Getter @Setter private String email;
	
}

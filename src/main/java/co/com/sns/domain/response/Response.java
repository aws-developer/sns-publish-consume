package co.com.sns.domain.response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> {
		
	private String systemData;
	
	private Integer status;
	
	private Integer errorCode;
	
	private String message;
	
	private List<String> errors;

	private T data;
	
	public void setSystemData() {
		Date date = new Date();
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		this.systemData = fmt.format(date);
	}

	public String getSystemData() {
		return systemData;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Response [systemData=" + systemData + ", status=" + status + ", errorCode=" + errorCode + ", message="
				+ message + ", errors=" + errors + ", data=" + data + "]";
	}
	
}

package co.com.sns.domain.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter 
@Setter
public class Message {
	private String tipoId;
	private String id;
}

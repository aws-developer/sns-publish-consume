package co.com.sns.utils;

import org.springframework.http.HttpStatus;

import co.com.sns.domain.response.Response;

public class Util {
	private Util() {
	}

	@SuppressWarnings("unchecked")
	public static <T> Response<T> callResponse(String msg, Object payload) {

		Response<T> response = new Response<T>();
		response.setData((T) payload);
		response.setMessage(msg);
		response.setStatus(HttpStatus.OK.value());
		return response;
	}
}
